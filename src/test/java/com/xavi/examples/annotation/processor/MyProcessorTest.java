package com.xavi.examples.annotation.processor;

import java.net.URL;
import java.util.Arrays;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyProcessorTest
{
    private JavaCompiler compiler;
    private StandardJavaFileManager fileManager;
    private MyProcessor myProcessor;
    DiagnosticCollector<? super JavaFileObject> myDiagnosticsListener;

    @Before
    public void setUp() throws Exception
    {
        compiler = ToolProvider.getSystemJavaCompiler();
        myDiagnosticsListener = new DiagnosticCollector<>();
        fileManager = compiler.getStandardFileManager(myDiagnosticsListener, null, null);
        myProcessor = new MyProcessor();
    }

    @After
    public void tearDown() throws Exception
    {
        fileManager.close();
    }

    private void compile(String resource)
    {
        URL location = MyProcessorTest.class.getClassLoader().getResource(resource);
        Iterable<? extends JavaFileObject> compilationUnit = fileManager.getJavaFileObjects(location.getFile());

        JavaCompiler.CompilationTask task = compiler.getTask(null, fileManager, myDiagnosticsListener, null, null, compilationUnit);
        task.setProcessors(Arrays.asList(myProcessor));
        task.call();
    }

    @Test
    public void testProcess() throws Exception
    {
        compile("MyAnnotatedClass.java");

        assertEquals(7, myProcessor.getTypes().size());
        assertEquals(7, myProcessor.getExpandedTypes().size());
        //...
    }

    @Test
    public void testWarning() throws Exception
    {
        compile("MyWarningClass.java");
        Diagnostic<?> diagnostic = myDiagnosticsListener.getDiagnostics().get(0);

        assertEquals(Diagnostic.Kind.WARNING, diagnostic.getKind());
        assertEquals("Using MyProcessor for private elements", diagnostic.getMessage(null));
    }
}