import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;

import com.xavi.examples.annotation.processor.MyAnnotation;

@MyAnnotation (visibility = Modifier.PRIVATE, elementKinds = ElementKind.FIELD)
public class MyWarningClass
{
}