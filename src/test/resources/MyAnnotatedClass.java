package com.xavi.checkmate.processor;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import javax.lang.model.element.ElementKind;

import com.xavi.examples.annotation.processor.MyAnnotation;

@MyAnnotation (elementKinds = {ElementKind.FIELD, ElementKind.METHOD})
public class MyAnnotatedClass<T extends CharSequence & Comparable<S>, S>
{
    public static Object myField;

    public T myField2;

    public int myField3;

    public T[] myField4;

    protected String myField5;

    public <A, B> void myMethod(HashMap<A, ? super T> a, String b) {}

    protected void myMethod2(String a, String b) {}

    protected <A extends CharSequence & Comparable<B>, B> void myMethod3(A a, String b) {}

    protected <K, V> void myMethod4(HashMap<K,V> a, String b) {}
}
