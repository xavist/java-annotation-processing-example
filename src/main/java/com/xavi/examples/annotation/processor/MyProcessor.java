package com.xavi.examples.annotation.processor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

@SupportedSourceVersion (SourceVersion.RELEASE_8)
@SupportedAnnotationTypes ("com.xavi.examples.annotation.processor.MyAnnotation")
public class MyProcessor extends AbstractProcessor
{
    private List<TypeMirror> types = new ArrayList<>();
    private List<String> expandedTypes = new ArrayList<>();
    MyElementVisitor myElementVisitor = new MyElementVisitor();
    MyTypeVisitor myTypeVisitor = new MyTypeVisitor();

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
    {
        for (Element e : roundEnv.getElementsAnnotatedWith(MyAnnotation.class))
        {
            MyAnnotation myAnnotation = e.getAnnotation(MyAnnotation.class);

            if(Modifier.PRIVATE == myAnnotation.visibility())
            {
                processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, "Using MyProcessor for private elements");
            }

            MyFilter myFilter = new MyFilter(myAnnotation.visibility(), myAnnotation.elementKinds());

            types.addAll(
                    e.getEnclosedElements().stream()
                            .flatMap(ee -> myElementVisitor.visit(ee, myFilter).stream())
                            .collect(Collectors.toList())
            );

            expandedTypes.addAll(
                    types.stream()
                            .map(myTypeVisitor::visit)
                            .collect(Collectors.toList())
            );
        }

        return true;
    }

    List<TypeMirror> getTypes()
    {
        return types;
    }

    List<String> getExpandedTypes()
    {
        return expandedTypes;
    }
}

