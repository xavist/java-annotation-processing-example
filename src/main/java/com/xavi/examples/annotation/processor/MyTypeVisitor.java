package com.xavi.examples.annotation.processor;

import java.util.stream.Collectors;

import javax.lang.model.element.TypeElement;
import javax.lang.model.type.ArrayType;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.IntersectionType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.lang.model.type.UnionType;
import javax.lang.model.type.WildcardType;
import javax.lang.model.util.SimpleTypeVisitor8;

class MyTypeVisitor extends SimpleTypeVisitor8<String, Void>
{
    protected MyTypeVisitor()
    {
        super("~");
    }

    @Override
    protected String defaultAction(TypeMirror t, Void aVoid)
    {
        return t.toString();
    }

    @Override
    public String visitIntersection(IntersectionType t, Void aVoid)
    {
        return t.getBounds().stream().map(this::visit).collect(Collectors.joining(" & "));
    }

    @Override
    public String visitUnion(UnionType t, Void aVoid)
    {
        return t.getAlternatives().stream().map(this::visit).collect(Collectors.joining(" | "));
    }

    @Override
    public String visitArray(ArrayType t, Void aVoid)
    {
        return visit(t.getComponentType()) + "[]";
    }

    @Override
    public String visitDeclared(DeclaredType t, Void aVoid)
    {
        String name = ((TypeElement)t.asElement()).getQualifiedName().toString();

        if(!t.getTypeArguments().isEmpty())
        {
            name += t.getTypeArguments().stream().map(this::visit).collect(Collectors.joining(", ", "<", ">"));
        }

        return name;
    }

    @Override
    public String visitTypeVariable(TypeVariable t, Void aVoid)
    {
        String name = t.asElement().getSimpleName().toString();

        if (t.getLowerBound().getKind() != TypeKind.NULL)
        {
            name += " super " + visit(t.getLowerBound());
        }

        if (!t.getUpperBound().toString().equals("java.lang.Object"))
        {
            name += " extends " + visit(t.getUpperBound());
        }

        return name;
    }

    @Override
    public String visitWildcard(WildcardType t, Void aVoid)
    {
        String name = "?";

        if (t.getSuperBound() != null)
        {
            name += " super " + visit(t.getSuperBound());
        }

        if (t.getExtendsBound() != null)
        {
            name += " extends " + visit(t.getExtendsBound());
        }

        return name;
    }
}
