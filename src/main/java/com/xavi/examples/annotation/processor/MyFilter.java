package com.xavi.examples.annotation.processor;

import java.util.Arrays;
import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;

public class MyFilter
{
    private final Modifier modifier;
    private final List<ElementKind> elementKinds;

    public MyFilter(Modifier modifier, ElementKind[] elementKinds)
    {
        this.modifier = modifier;
        this.elementKinds = Arrays.asList(elementKinds);
    }

    public boolean shouldRetrieveTypesFor(Element e)
    {
        return e.getModifiers().contains(modifier) && elementKinds.contains(e.getKind());
    }

}
