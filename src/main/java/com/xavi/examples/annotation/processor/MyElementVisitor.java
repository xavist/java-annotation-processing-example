package com.xavi.examples.annotation.processor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.SimpleElementVisitor8;

class MyElementVisitor extends SimpleElementVisitor8<List<TypeMirror>, MyFilter>
{
    public MyElementVisitor()
    {
        super(Collections.emptyList());
    }

    @Override
    protected List<TypeMirror> defaultAction(Element e, MyFilter myFilter)
    {
        if (myFilter.shouldRetrieveTypesFor(e))
        {
            return Collections.singletonList(e.asType());
        }

        return DEFAULT_VALUE;
    }

    @Override
    public List<TypeMirror> visitExecutable(ExecutableElement e, MyFilter myFilter)
    {
        if (myFilter.shouldRetrieveTypesFor(e))
        {
            List<TypeMirror> list = new ArrayList<>();
            list.add(e.getReturnType());
            e.getParameters().stream().map(Element::asType).forEach(list::add);

            return list;
        }

        return DEFAULT_VALUE;
    }
}
