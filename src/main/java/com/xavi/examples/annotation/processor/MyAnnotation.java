package com.xavi.examples.annotation.processor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;


@Target(ElementType.TYPE)
public @interface MyAnnotation
{
    Modifier visibility() default Modifier.PUBLIC;

    ElementKind[] elementKinds();
}